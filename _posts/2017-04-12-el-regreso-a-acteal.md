
<html>
<head>
<meta charset="utf-8" />
<!-- Website Design By: www.happyworm.com -->
<title>Los Doraditos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="/dist/skin/blue.monday/css/jplayer.blue.monday.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/lib/jquery.min.js"></script>
<script type="text/javascript" src="/dist/jplayer/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="/dist/add-on/jplayer.playlist.min.js"></script>
<script type="text/javascript">
//<![CDATA[
$(document).ready(function(){

	var myPlaylist = new jPlayerPlaylist({
		jPlayer: "#jquery_jplayer_N",
		cssSelectorAncestor: "#jp_container_N"
	}, [
		{
		   title:"Adagio de Marcello",
				artist:"Quetzal Eckhart",
				mp3:"http://data.elmisterio.org/music/Contemplations-On-A-Quena/adagio.mp3",
				poster: "http://data.elmisterio.org/assets/images/9.jpg"
		}
	], {
		playlistOptions: {
			enableRemoveControls: true
		},
		swfPath: "/dist/jplayer",
		supplied: "webmv, ogv, m4v, oga, mp3",
		useStateClassSkin: true,
		autoBlur: false,
		smoothPlayBar: true,
		keyEnabled: true,
		audioFullScreen: true
	});

	// Click handlers for jPlayerPlaylist method demo

	// Audio mix playlist

	$("#playlist-setPlaylist-audio-mix").click(function() {
		myPlaylist.setPlaylist([
			{
			title:"Pictures At An Exhibition - Mussorgsky",
			artist:"Quetzal Eckhart",
			mp3:"http://data.elmisterio.org/music/Contemplations-On-A-Quena/pictures-at-an-exhibition.mp3",
			poster: "http://elmisterio.org/assets/images/cross-quena.jpg"
			},
			{				
				title:"Adagio",
			artist:"Quetzal Eckhart",
			mp3:"http://data.elmisterio.org/music/Contemplations-On-A-Quena/adagio.mp3",
			poster: "http://elmisterio.org/assets/images/cross-quena.jpg"
			},
			{
					title:"Andalouse",
			artist:"Quetzal Eckhart",
			mp3:"http://data.elmisterio.org/music/Contemplations-On-A-Quena/andalouse.mp3",
			poster: "http://elmisterio.org/assets/images/cross-quena.jpg"
			},
			{
					title:"Meditation",
			artist:"Quetzal Eckhart",
			mp3:"http://data.elmisterio.org/music/Contemplations-On-A-Quena/meditation.mp3",
			poster: "http://elmisterio.org/assets/images/cross-quena.jpg"
			},
			{
				title:"Dvorak",
			artist:"Quetzal Eckhart",
			mp3:"http://data.elmisterio.org/music/Contemplations-On-A-Quena/dvorak.mp3",
			poster: "http://elmisterio.org/assets/images/cross-quena.jpg"
			},
			{
                title:"Whistles",
			artist:"Quetzal Eckhart",
			mp3:"http://data.elmisterio.org/music/Contemplations-On-A-Quena/whistles.mp3",
			poster: "http://elmisterio.org/assets/images/cross-quena.jpg"
			},
			{
                title:"John Dowland Songs",
			artist:"Quetzal Eckhart",
			mp3:"http://data.elmisterio.org/music/Contemplations-On-A-Quena/dowland.mp3",
			poster: "http://elmisterio.org/assets/images/cross-quena.jpg"
			},
			{
                title:"Nocturne - Chopin",
			artist:"Quetzal Eckhart",
			mp3:"http://data.elmisterio.org/music/Contemplations-On-A-Quena/nocturne.mp3",
			poster: "http://elmisterio.org/assets/images/cross-quena.jpg"
			},
			{
                title:"Anandamurti Melodies",
			artist:"Quetzal Eckhart",
			mp3:"http://data.elmisterio.org/music/Contemplations-On-A-Quena/anandamurti.mp3",
			poster: "http://elmisterio.org/assets/images/cross-quena.jpg"
			},
			{
                title:"William Enckhausen plays Heinrich Enckhausen, Handel, and Telemann",
			artist:"Quetzal Eckhart",
			mp3:"http://data.elmisterio.org/music/Contemplations-On-A-Quena/enckhausen.mp3",
			poster: "http://elmisterio.org/assets/images/cross-quena.jpg"
			},
			{
                title:"Reverie - Debussy",
			artist:"Quetzal Eckhart",
			mp3:"http://data.elmisterio.org/music/Contemplations-On-A-Quena/reverie.mp3",
			poster: "http://elmisterio.org/assets/images/cross-quena.jpg"
			},
			{
                title:"Dance Of The Blessed Spirits",
			artist:"Quetzal Eckhart",
			mp3:"http://data.elmisterio.org/music/Contemplations-On-A-Quena/blessed-spirits.mp3",
			poster: "http://elmisterio.org/assets/images/cross-quena.jpg"
			},
			{
                title:"Los Doraditos",
			artist:"Quetzal Eckhart",
			mp3:"http://data.elmisterio.org/music/Contemplations-On-A-Quena/los-doraditos.mp3",
			poster: "http://elmisterio.org/assets/images/cross-quena.jpg"                                                                                     
			}
		]);
	});

	// Video mix playlist

	$("#playlist-setPlaylist-video-mix").click(function() {
		myPlaylist.setPlaylist([
			{
			    title:"Govinda",
				artist:"El Misterio",
				mp3:"http://data.elmisterio.org/music/Kiirtan-El-Misterio/govinda.mp3",
				poster: "http://elmisterio.org/assets/images/kiirtan.jpg"
			},
            {
			    title:"Topilejo",
				artist:"El Misterio",
				mp3:"http://data.elmisterio.org/music/Kiirtan-El-Misterio/topilejo.mp3",
				poster: "http://elmisterio.org/assets/images/kiirtan.jpg"
			},
			{
				title:"Padmasambhava",
				artist:"El Misterio",
				mp3:"http://data.elmisterio.org/music/Kiirtan-El-Misterio/padmasambhava.mp3",
				poster: "http://elmisterio.org/assets/images/kiirtan.jpg"
			},
			{
				title:"Baba Nam Kevalam",
				artist:"El Misterio",
				mp3:"http://data.elmisterio.org/music/Kiirtan-El-Misterio/babanamkevalam.mp3",
				poster: "http://elmisterio.org/assets/images/kiirtan.jpg"
			},
			{
				title:"Soja",
				artist:"El Misterio",
				mp3:"http://data.elmisterio.org/music/Kiirtan-El-Misterio/soja.mp3",
				poster: "http://elmisterio.org/assets/images/kiirtan.jpg"
			},
			{
				title:"Om Ah Hum Vajra Guru",
				artist:"El Misterio",
				mp3:"http://data.elmisterio.org/music/Kiirtan-El-Misterio/om-ah-hum-vajra-guru.mp3",
				poster: "http://elmisterio.org/assets/images/kiirtan.jpg"
			},
			{
				title:"Nikte Ha Kiirtan",
				artist:"El Misterio",
				mp3:"http://data.elmisterio.org/music/Kiirtan-El-Misterio/nikteha.mp3",
				poster: "http://elmisterio.org/assets/images/kiirtan.jpg"
			},
			{
				title:"Reverie Kiirtan",
				artist:"El Misterio",
				mp3:"http://data.elmisterio.org/music/Kiirtan-El-Misterio/reverie-kiirtan.mp3",
				poster: "http://elmisterio.org/assets/images/kiirtan.jpg"
			},
			{
				title:"Desierto",
				artist:"El Misterio",
				mp3:"http://data.elmisterio.org/music/Kiirtan-El-Misterio/desierto2.mp3",
				poster: "http://elmisterio.org/assets/images/kiirtan.jpg"
			},
			{
				title:"Tiny Green Island",
				artist:"El Misterio",
				mp3:"http://data.elmisterio.org/music/Kiirtan-El-Misterio/tiny-green-island.mp3",
				poster: "http://elmisterio.org/assets/images/kiirtan.jpg"
			},
			{
				title:"La Gracia",
				artist:"El Misterio",
				mp3:"http://data.elmisterio.org/music/Kiirtan-El-Misterio/gracia.mp3",
				poster: "http://elmisterio.org/assets/images/kiirtan.jpg"
			},
			{
				title:"Los Doraditos",
				artist:"El Misterio",
				mp3:"http://data.elmisterio.org/music/Kiirtan-El-Misterio/los-doraditos.mp3",
				poster: "http://elmisterio.org/assets/images/kiirtan.jpg"
			}
		]);
	});

	// Media mix playlist

	$("#playlist-setPlaylist-media-mix").click(function() {
		myPlaylist.setPlaylist([
			{
				title:"Gavotte And Minuet",
				artist:"Quetzal Eckhart",
				mp3:"http://data.elmisterio.org/music/Bach-On-Bamboo/gavotte-minuet.mp3",
				poster: "http://elmisterio.org/assets/images/cross-quena.jpg"
			},
			{
				title:"Air and Gavotte",
				artist:"Quetzal Eckhart",
				mp3:"http://data.elmisterio.org/music/Bach-On-Bamboo/air-gavotte.mp3",
				poster: "http://elmisterio.org/assets/images/cross-quena.jpg"
			},
			{
				title:"Christmas Oratorio",
				artist:"Quetzal Eckhart",
				mp3:"http://data.elmisterio.org/music/Bach-On-Bamboo/christmas-oratorio.mp3",
				poster: "http://elmisterio.org/assets/images/cross-quena.jpg"
			},
			{
				title:"Sonata in B-minor",
				artist:"Quetzal Eckhart",
				mp3:"http://data.elmisterio.org/music/Bach-On-Bamboo/sonata-b-minor.mp3",
				poster: "http://elmisterio.org/assets/images/cross-quena.jpg"
			},
			{
				title:"Minuet, Air, and Bouree",
				artist:"Quetzal Eckhart",
				mp3:"http://data.elmisterio.org/music/Bach-On-Bamboo/minuet-air-bouree.mp3",
				poster: "http://elmisterio.org/assets/images/cross-quena.jpg"
			}
		]);
	});

	


	// The remove commands

	$("#playlist-remove").click(function() {
		myPlaylist.remove();
	});

	$("#playlist-remove--2").click(function() {
		myPlaylist.remove(-2);
	});
	$("#playlist-remove--1").click(function() {
		myPlaylist.remove(-1);
	});
	$("#playlist-remove-0").click(function() {
		myPlaylist.remove(0);
	});
	$("#playlist-remove-1").click(function() {
		myPlaylist.remove(1);
	});
	$("#playlist-remove-2").click(function() {
		myPlaylist.remove(2);
	});

	// The shuffle commands

	$("#playlist-shuffle").click(function() {
		myPlaylist.shuffle();
	});

	$("#playlist-shuffle-false").click(function() {
		myPlaylist.shuffle(false);
	});
	$("#playlist-shuffle-true").click(function() {
		myPlaylist.shuffle(true);
	});

	// The select commands

	$("#playlist-select--2").click(function() {
		myPlaylist.select(-2);
	});
	$("#playlist-select--1").click(function() {
		myPlaylist.select(-1);
	});
	$("#playlist-select-0").click(function() {
		myPlaylist.select(0);
	});
	$("#playlist-select-1").click(function() {
		myPlaylist.select(1);
	});
	$("#playlist-select-2").click(function() {
		myPlaylist.select(2);
	});

	// The next/previous commands

	$("#playlist-next").click(function() {
		myPlaylist.next();
	});
	$("#playlist-previous").click(function() {
		myPlaylist.previous();
	});

	// The play commands

	$("#playlist-play").click(function() {
		myPlaylist.play();
	});

	$("#playlist-play--2").click(function() {
		myPlaylist.play(-2);
	});
	$("#playlist-play--1").click(function() {
		myPlaylist.play(-1);
	});
	$("#playlist-play-0").click(function() {
		myPlaylist.play(0);
	});
	$("#playlist-play-1").click(function() {
		myPlaylist.play(1);
	});
	$("#playlist-play-2").click(function() {
		myPlaylist.play(2);
	});

	// The pause command

	$("#playlist-pause").click(function() {
		myPlaylist.pause();
	});

	// Changing the playlist options

	// Option: autoPlay

	$("#playlist-option-autoPlay-true").click(function() {
		myPlaylist.option("autoPlay", true);
	});
	$("#playlist-option-autoPlay-false").click(function() {
		myPlaylist.option("autoPlay", false);
	});

	// Option: enableRemoveControls

	$("#playlist-option-enableRemoveControls-true").click(function() {
		myPlaylist.option("enableRemoveControls", true);
	});
	$("#playlist-option-enableRemoveControls-false").click(function() {
		myPlaylist.option("enableRemoveControls", false);
	});

	// Option: displayTime

	$("#playlist-option-displayTime-0").click(function() {
		myPlaylist.option("displayTime", 0);
	});
	$("#playlist-option-displayTime-fast").click(function() {
		myPlaylist.option("displayTime", "fast");
	});
	$("#playlist-option-displayTime-slow").click(function() {
		myPlaylist.option("displayTime", "slow");
	});
	$("#playlist-option-displayTime-2000").click(function() {
		myPlaylist.option("displayTime", 2000);
	});

	// Option: addTime

	$("#playlist-option-addTime-0").click(function() {
		myPlaylist.option("addTime", 0);
	});
	$("#playlist-option-addTime-fast").click(function() {
		myPlaylist.option("addTime", "fast");
	});
	$("#playlist-option-addTime-slow").click(function() {
		myPlaylist.option("addTime", "slow");
	});
	$("#playlist-option-addTime-2000").click(function() {
		myPlaylist.option("addTime", 2000);
	});

	// Option: removeTime

	$("#playlist-option-removeTime-0").click(function() {
		myPlaylist.option("removeTime", 0);
	});
	$("#playlist-option-removeTime-fast").click(function() {
		myPlaylist.option("removeTime", "fast");
	});
	$("#playlist-option-removeTime-slow").click(function() {
		myPlaylist.option("removeTime", "slow");
	});
	$("#playlist-option-removeTime-2000").click(function() {
		myPlaylist.option("removeTime", 2000);
	});

	// Option: shuffleTime

	$("#playlist-option-shuffleTime-0").click(function() {
		myPlaylist.option("shuffleTime", 0);
	});
	$("#playlist-option-shuffleTime-fast").click(function() {
		myPlaylist.option("shuffleTime", "fast");
	});
	$("#playlist-option-shuffleTime-slow").click(function() {
		myPlaylist.option("shuffleTime", "slow");
	});
	$("#playlist-option-shuffleTime-2000").click(function() {
		myPlaylist.option("shuffleTime", 2000);
	});

	// Equivalent commands

	$("#playlist-equivalent-1-a").click(function() {
		myPlaylist.add({
			title:"Your Face",
			artist:"The Stark Palace",
			mp3:"http://www.jplayer.org/audio/mp3/TSP-05-Your_face.mp3",
			oga:"http://www.jplayer.org/audio/ogg/TSP-05-Your_face.ogg",
			poster: "http://www.jplayer.org/audio/poster/The_Stark_Palace_640x360.png"
		}, true);
	});

	$("#playlist-equivalent-1-b").click(function() {
		myPlaylist.add({
			title:"Your Face",
			artist:"The Stark Palace",
			mp3:"http://www.jplayer.org/audio/mp3/TSP-05-Your_face.mp3",
			oga:"http://www.jplayer.org/audio/ogg/TSP-05-Your_face.ogg",
			poster: "http://www.jplayer.org/audio/poster/The_Stark_Palace_640x360.png"
		});
		myPlaylist.play(-1);
	});

	// AVOID COMMANDS

	$("#playlist-avoid-1").click(function() {
		myPlaylist.remove(2); // Removes the 3rd item
		myPlaylist.remove(3); // Ignored unless removeTime=0: Where it removes the 4th item, which was originally the 5th item.
	});


});
//]]>
</script>
</head>
<body>
<p style="margin-top:1em;">
				

<div id="jp_container_N" class="jp-video jp-video-270p" role="application" aria-label="media player">
	<div class="jp-type-playlist">
		<div id="jquery_jplayer_N" class="jp-jplayer"></div>
		<div class="jp-gui">
			<div class="jp-video-play">
				<button class="jp-video-play-icon" role="button" tabindex="0">play</button>
			</div>
			<div class="jp-interface">
				<div class="jp-progress">
					<div class="jp-seek-bar">
						<div class="jp-play-bar"></div>
					</div>
				</div>
				<div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
				<div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
				<div class="jp-controls-holder">
					<div class="jp-controls">
						<button class="jp-previous" role="button" tabindex="0">previous</button>
						<button class="jp-play" role="button" tabindex="0">play</button>
						<button class="jp-next" role="button" tabindex="0">next</button>
						<button class="jp-stop" role="button" tabindex="0">stop</button>
					</div>
					<div class="jp-volume-controls">
						<button class="jp-mute" role="button" tabindex="0">mute</button>
						<button class="jp-volume-max" role="button" tabindex="0">max volume</button>
						<div class="jp-volume-bar">
							<div class="jp-volume-bar-value"></div>
						</div>
					</div>
					<div class="jp-toggles">
						<button class="jp-repeat" role="button" tabindex="0">repeat</button>
						<button class="jp-shuffle" role="button" tabindex="0">shuffle</button>
						<button class="jp-full-screen" role="button" tabindex="0">full screen</button>
					</div>
				</div>
				<div class="jp-details">
					<div class="jp-title" aria-label="title">&nbsp;</div>
				</div>
			</div>
		</div>
		<div class="jp-playlist">
			<ul>
				<!-- The method Playlist.displayPlaylist() uses this unordered list -->
				<li>&nbsp;</li>
			</ul>
		</div>
		<div class="jp-no-solution">
			<span>Update Required</span>
			To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
		</div>
	</div>
</div>
			
&nbsp;


&nbsp;
<code></code><br />
&nbsp;
<li><a href="http://data.elmisterio.org/music/Contemplations-On-A-Quena/adagio.mp3">Download Adagio de Marcello</a></li>
&nbsp;
&nbsp;

<li>Yo estaba practicando esta pieza con la quena cuando visitemos Acteal.</li>
&nbsp;

<li>En nuestra primera visita a Acteal conocí a un hombre que perdió a toda su familia en <a href="https://es.wikipedia.org/wiki/Matanza_de_Acteal">la matanza de 1997.</a> Yo había leído sobre este incidente desde hace años y tuve la oportunidad un serie de preguntas a este líder de la comunidad acerca de lo que sucedido en '97. Se confirmó que eran un base de apoyo zapatista en aquel tiempos. Sin embargo, había habido acuerdos de paz desde el 94. Ellos creen que el ejército federal apoya fuerzas paramilitares locales que comitieron el masacre y siguen aterrorizando la gente de Acteal. ¿En que otra manera pueden tener armas de alta potencia e ir libremente aterrorizar a la gente mientras hay bases militares cercanas que podrían potencialmente controlar estos criminales? Es obvio que los paramilitares hacen el trabajo sucio de los militares federales y continúan sin obstáculos en su terror y la tiranía. Todas las víctimas de 97 estaban indefensas e incluía en su mayoría mujeres y niños. Todavía ocurren masacres más pequeños en la zona y emboscadas regulares contra ellos.</li>
&nbsp;

<li><img src="http://elmisterio.org/images/martyrs2.jpg" alt="neem" width="646" height="960" class="alignnone size-full wp-image-1039" />

<img src="http://elmisterio.org/images/van-martyrs.jpg"  width="646" height="960" class="alignnone size-full wp-image-1039" />

&nbsp;

<li>Acteal se mueve hacia adelante. Ahora tienen su propia sociedad civil “Las Abejas de Acteal”. Visitamos una parte de la comunidad que proporciona alojamiento a las víctimas que han perdido sus tierras. Mi esposa utiliza la clinica de ellos para dar tratamientos naturistas. Son personas muy nobles y es simplemente increíble ver que todavía tienen un poco de alegría en sus vidas a pesar de vivir bajo la constante amenaza de la aniquilación. Pude ver algunas de las cualidades espirituales muy notables en algunos de los líderes de la comunidad y ver que estas personas han realizado la gran responsabilidad de curación y guiar a la comunidad hacia adelante.</li>

&nbsp;

<li><img src="http://elmisterio.org/images/clinic.jpg" alt="neem" width="646" height="960" class="alignnone size-full wp-image-1039" />

<img src="http://elmisterio.org/images/acteal-preists.jpg" alt="neem" width="646" height="960" class="alignnone size-full wp-image-1039" />

&nbsp;

<li>En realidad, la primera vez que visité a Acteal fue en 1998. Era en un sueño y no sabía que era Acteal. En el sueño vi lo que parecían cuerpos ennegrecidos y carbonizados de muchas personas amontonadas. En el sueño di cuenta de que nunca volvería a leer otro libro y tiré un volumen de Emerson en la basura.  Solo puedo tratar de adaptarme a este sentimiento abrumador de compasión infinita simultánea con la tormenta, la bendición con la maldición, y el cielo con el infierno. Sabía que el corazón y la compasion era el único camino después de este sueño. Me di cuenta que todo se puede quitar en esta vida impermanente menos estos regalos espirituales del infinito de conocimiento compasivo.  Casi 20 años después visito Acteal y veo la estatua de los 45 mártires que allí murieron. Las figuras en la esatua parecen como las figuras ennegrecidos y carbonizados de mi sueño.  Recuerdo inmediatamente el sueño de 20 años anterior y sentí exactamente los mismos sentimientos que sentí en el sueño. Durante la primera hora no pude sacar de mi mente el sentimiento desesperanzador. Luego vinieron los sacerdotes "Jedi", los sacerdotes indígenas vestidos con ropa blanca simple. Parecen franciscanos pero llevaban una túnica de 1 pieza como Jedis, solo sin botas altas pero con chancletas. Tocemos un poco de música con ellos, mientras que Giitanjali curó a la gente y hicimos amigos.
</li>
&nbsp;

<li>La liberación espiritual se logra a través de<em> Sadhana y Seva</em>. <em>Sadhana</em> significa "hacer un esfuerzo sincero" en el esfuerzo para meditar y conocer la Conciencia Suprema en su interior. <em>Seva</em> es el servicio a todos los seres vivos como expresiónes de la Conciencia Suprema. Como vemos más de la Conciencia Suprema dentro, hay que conectar más con las expresiones de la Conciencia Suprema en otros, especialmente los que faltan las necesidades basicas para la existencia. El resultado final de la meditación y el servicio es el mismo: la realización de la unión de todas las almas con la Conciencia Suprema. Nuestros proyectos de servicio social con las comunidades indígenas de México es una forma de seva.   Nosotros llevamos alimentos y suministros al refugio en Acteal para la gente local que han perdido sus tierras y casas. Tambien damos tratamientos y medicamentos naturopaticos.  </li>

&nbsp;

<li><img src="http://elmisterio.org/images/martyrs.jpg" alt="neem" width="646" height="960" class="alignnone size-full wp-image-1039" />



&nbsp;

<li>En el sueño vi lo que parecían cuerpos ennegrecidos y carbonizados de muchas personas amontonadas. En el sueño di cuenta de que nunca volvería a leer otro libro y tiré un volumen de Emerson en la basura.  Solo puedo tratar de adaptarme a este sentimiento abrumador de compasión infinita simultánea con la tormenta, la bendición con la maldición, y el cielo con el infierno. Sabía que el corazón y la compasion era el único camino después de este sueño. Todo se puede quitar en la vida menos estos regalos espirituales del infinito.  Casi 20 años después visito Acteal y veo la estatua de los 45 mártires que allí murieron. Las figuras en la esatua parecen como las figuras ennegrecidos y carbonizados de mi sueño.
</li>


&nbsp;
<li>En el sueño di cuenta de que nunca volvería a leer otro libro y tiré un volumen de Emerson en la basura.  Solo puedo tratar de adaptarme a este sentimiento abrumador de compasión infinita simultánea con la tormenta, la bendición con la maldición, y el cielo con el infierno. Sabía que el corazón y la compasion era el único camino después de este sueño. Todo se puede quitar en la vida menos estos regalos espirituales del infinito.  Casi 20 años después visito Acteal y veo la estatua de los 45 mártires que allí murieron. Las figuras en la esatua parecen como las figuras ennegrecidos y carbonizados de mi sueño.</li>
</li>


&nbsp;
<li>escucha otros albumes y pistas:  <a href="http://elmisterio.org/es/la-flauta-de-bambu"> La Flauta De Bambu</a></li>












