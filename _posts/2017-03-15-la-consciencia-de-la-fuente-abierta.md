Escribo sobre el Tantra Maya desde las perspectivas teórica y práctico. Trato de mantener el aspecto práctico muy simple. En lugar de enseñar prácticas complejas y secretas a las personas, he desarrollado ciertas prácticas generales y públicas que puedo enseñar a personas que son sinceras y morales, que tienen amor por la humanidad, la naturaleza y el universo. Aunque todavía reconozco el valor de las iniciaciones y prácticas secretas que deberían ser públicamente retenidas, prefiero enseñar prácticas sencillas. Esto es especialmente mi deseo al compartir literatura sobre Tantra Maya. Trato de mantener los argumentos filosóficos simples y prácticos y en su lugar se centran más en la psicología pragmática y las meditaciones simples. Se supone que el tantra es una ciencia intuitiva que sigue métodos prácticos que son revisados ​​por otros compañeros. La historia del tantra no es más que la historia de la práctica espiritual, o "empirismo místico", como lo llamó Aurobindu. Trato de mantener ese espíritu puro y experimental en mis escritos para evitar la trampa de establecer creencias, rituales o dogmas que no están basados ​​en la experiencia real de uno, que no son universalmente aplicables a la humanidad en general. La práctica espiritual es como un "software mental" en que tiene una cierta programación con una determinada función. Esta función debe ser racional, humana y equilibrada y no debe contener elementos peligrosos que puedan perjudicar al médico, ya sea que se trate de meditación o simplemente familiarizarse con la literatura sobre el tema. Tal programa mental debe ser de "fuente abierta", abierto a la crítica y revisión por los practicantes. La religión y el dogma politica se puede ver como una programa muy rígido, "de código cerrado" que no puede ser cuestionado o investigado. Este programa hace que la mente funcione ciegamente y sólo siga algunos funciones y comandos de que uno no es consciente. Controlan tu pensamiento, valores e incluso sentimientos, pero no eres consciente de cómo lo hacen. ¿Lo utilizas, o te utiliza?  ¿Eres la maquina o el controlador de la maquina?

También estudio y escribo sobre 2 revoluciones actuales y vivas; el movimiento de Ananda Marga en la India y el <a href="http://elmisterio.org/es/hacia-el-zapatismo">movimiento zapatista en México</a> "Hacia el zapatismo", el vínculo que acabamos de referir, explica un poco sobre estos movimientos.  Tal vez me acerqué un poco demasiado a Ananda Marga y algunas personas quieren saber por que escribo sobre los Zapatistas.  Es facil entender como "Hermano Grande" no quiere que buena informacion de estas movimientos circula libre por internet.  

Quizás por esta razón siempre me atacan con malware muy peligroso hecho para destruir el sistema operativo de uno cuando uso servidores de datos comerciales y corporativos como Facebook y Google. El Internet es un gran vicio con el control meta-corporativo que Facebook, Google y otros tienen sobre él, pero también puede ser una gran herramienta a través del medio adecuado. Un Internet libre y justo que respete la privacidad requiere software de "codigo abierto", o <a href="https://fsf.org"> Software Libre. </a> He comenzado a ver el paradigma de "código abierto" como muy similar al paradigma que tengo para compartir mi literatura sobre el tantra. Al igual que el software, la práctica espiritual no debe tener trucos ocultos o tendencias y debe ser tan "espiritualmente científico" como sea posible. Considerando la similitud de ambos paradigmas, llamo a esta idea "La Conciencia de la Fuente Abierta", una idea que puede aplicarse tanto al software técnico como al "software" mental de las ideas.

He intentado mover el foro a Diáspora en lugar de Facebook, pero todo del mundo usa Facebook. Todos modos, voy a compartir esa informacion sobre un red social alternativa. Diaspora es una red social pura que utiliza software de código abierto, cuyo código fuente puede ser verificado públicamente por expertos para no contener spyware, publicidad o algoritmos manipulados. 

<p>La informacion que sigue es algo que yo he modificado y adaptado ligeramente desde: <a href="https://diasporafoundation.org/about">The Diaspora Foundation</a></p>

<p><strong>Red Decentralizada</strong> (Diaspora)
<img class="alignnone size-medium wp-image-4118" src="https://diasporafoundation.org/assets/pages/about/network-distributed-4a1319210dde0eeac67a7e7562912dc3377f6a13a71aec7857e58e3fc44e12d8.png" alt="" width="300" height="120" /></p>

<p>Diaspora* es una verdadera red social, sin base central. Hay servidores (llamados “vainas”) en todo el mundo, cada uno de los cuales contiene los datos de los usuarios que han decidido registrarse con él. Estos pods se comunican entre sí con encripcion total (end-to-end encryption), para que pueda registrarse con cualquier pod y comunicarse libremente con sus contactos, dondequiera que estén en la red.</p>

<p><strong>Red Centralizada</strong> (Facebook)</p>

<p><img class="alignnone size-medium wp-image-4119" src="https://diasporafoundation.org/assets/pages/about/network-centralized-e0e48aac267a43dd146d43374f3d0539701e9030da9ab82af4e30adb4d79d908.png" alt="" width="300" height="120" /></p>

<p>La mayoría de las redes sociales se ejecutan desde servidores centralizados propiedad y administrados por una corporación.
Estos almacenan todos los datos privados de sus usuarios. Esta información puede ser perdida o hackeada,y como cualquier sistema con un cuello de botella, cualquier problema en los servidores centrales puede hacer que toda la red se ejecuta muy lentamente, o no en absoluto. También es más fácil para los gobiernos “escuchar.”</p>

<div class="col-md-2">
<div class="page-header">
<h1>Como conectarme?</h1>
</div>
<div class="container">
<div class="row">
<div class="col-md-12">

A pesar de que Diáspora* se compone de muchas vainas en todo el mundo,
Lo experimentará como una red integrada. No necesitas
estar en la misma vaina que sus contactos para comunicarse
libremente entre sí - la comunicación se realiza sin problemas
con todas las vainas del universo Diáspora*. Cuando usas 
Diáspora*, usted puede fácilmente olvidar que en realidad se compone de muchas vainas.
Yo recomiendo las vainas:  pod.orkz.net, flokk.no, y diasp.de.  
Nunca he tenido problemas en estas vainas.
El sitio, <a href="https://pod.orkz.net">pod.orkz.net</a> esta ubicado en un servidor
en Holanda donde la libertad de expresion y la privacidad es mas protegido por leyes.


Conectar con alguien en Diáspora* es realmente muy simple:

</div>
</div>
<div class="row">
<div class="col-md-2">
<p class="text-center"><img src="https://diasporafoundation.org/assets/pages/about/search-48804a7bef32cd2948a34e8a07283cf1100f408c2668bfdd92d47559ce7378ea.png" alt="Search" /></p>

</div>
<div class="col-md-4">
<h3>1. Encontrarlos</h3>
Todo lo que necesitas hacer para conectarte a alguien es encontrarlos y agregarlos a
un aspecto. (Vea más abajo para más detalles sobre los aspectos.) Encuentrelos usando el búsqueda en la barra negra, o coloque el cursor sobre su nombre en su
avatar aparecerá. Si conoce su identificacion de Diáspora * (su nombre@nombrepod.com),
usted puede utilizar eso. 
</div>
<div class="col-md-2">
<p class="text-center"><img src="https://diasporafoundation.org/assets/pages/about/plus-ec8d90d51965b00e44a53747c1af6f66975f41a0a01470fabe39e42624088c2d.png" alt="Add a contact" /></p>

</div>
<div class="col-md-4">
<h3>2. Agregarlos</h3>
Entonces es sólo cuestión de hacer clic en el botón "Agregar contacto" y elegir
de qué aspecto quieres que formen parte. Ahora estás conectado y puedes
Compartir con ellos como lo haría en cualquier otra red. O, si son parte
de diferentes aspectos de su vida, añádalos a múltiples aspectos. Usted es
ahora conectado. Es tan fácil como eso.

</div>
</div>
</div>
</div>
<div class="container">
<div class="row">
<div id="freedom" class="page-header">
<h1>La Libertad <small> Diaspora* no te limita</small></h1>
</div>
<div class="container">
<div class="row">
<div class="col-md-4">
<h3>Sea quien quieres ser</h3>
Muchas redes insisten en que usen su identidad real. No en Diáspora*.
Aquí puedes elegir quién quieres ser y compartir tanto o tan poco
sobre ti como quieras. Realmente depende de ti cómo desea interactuar
con otras personas.

</div>
<div class="col-md-4">
<h3>Ser Creativa</h3>
No estás limitado a cómo puedes interactuar. Puedes seguir personas fascinantes para ver lo que tienen que decir, o puede compartir el mundo con todos.
Comparte tus fotos, ilustraciones, videos, música, palabras, lo que quieras. Déjate volar.

</div>
<div class="col-md-4">
<h3>Libre como La Libertad</h3>
Diáspora * es completamente Software Libre. Esto significa que no hay límites sobre cómo
puede ser usado. Incluso puede tomar el código fuente y cambiarlo para hacerlo
trabajar de la manera que desee y ayudarnos a mejorar la red. Nos encantaría
tenerte a bordo
<div class="container"></div>
<div id="privacy" class="page-header">
<h1>Privacy <small>Control total sobre lo que es tuyo </small></h1>
</div>
<div class="container">
<div class="row">
<div class="col-md-4">
<h3>Tus datos son tuyos</h3>
Muchas redes usan sus datos para ganar dinero analizando sus interacciones y
utilizando esta información para anunciarte cosas. Diaspora* no usa su
datos para cualquier propósito que no sea permitirle conectarse y compartir con otros.

</div>
<div class="col-md-4">
<h3>Hospedar Diaspora*</h3>
<h3>Elija donde se almacenan sus datos seleccionando una vaina con el que esté satisfecho. Si
desea estar realmente seguro, puede configurar y alojar su propio pod en los servidores
usted controla, así que nadie puede conseguir en sus datos personales.</h3>
<p class="desc-with-button"></p>

</div>
</div>
</div>
<div class="col-md-4"></div>
<div class="col-md-4">
<h3>Escoger su audencia</h3>
<p class="desc-with-button">Los aspectos de Diáspora* te permiten compartir con
aquellas personas que quieras. Puedes ser tan público o tan privado como quieras.
Comparta una foto divertida con todo el mundo, o un secreto profundo con su
amigos cercanos. Tienes el control.</p>

</div>
<p class="desc-with-button">Facebook siempre ha bloqueado mis cuentas en algunos
manera u otra y Diaspora* parece un lugar mucho mejor para un foro.
Diaspora utiliza etiquetas (#) to clasificar categorias de publicaciones.  Por ejemplo,
uno puede "seguir" las categorias de #naturaleza #fotografia #meditacion, etc. 

