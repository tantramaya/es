<iframe width="560" height="315" src="https://www.youtube.com/embed/oi_HRlNnWjI" frameborder="0" allowfullscreen></iframe>

Fui a la India en busca de más conocimiento. Allí conocí a Chidghananda, un monje viejo, solitario, muy venerado en
su orden. Era considerado un santo, y me sentí muy
honrado de que me aceptara como amigo. A veces yo lo
acompañaba con sus meditaciones en las tardes. Él
siempre escuchaba el sonido sagrado OM y en ese tiempo
aumentó en su meditación. Era obvio que él
experimentaba ananda regularmente, la bienaventuranza
divina. Él era realmente uno de los seres humanos más
amorosos que yo había conocido. Mis experiencias se
habían intensificado cerca de él en Ananda Nagar y era la
voluntad divina que yo hubiera conocido a un maestro
como el para guiarme a través de esos procesos difíciles.
En ese momento yo quería ser un monje, pero
Chidghananda me dijo que yo era un poco raro y no
encajaría bien en la organización monástica. 


Él dijo que mi trabajo espiritual se acercaba a su fin, y que en
realidad no tenía que hacer cualquier otra cosa con mi
vida más que meditar, vivir con sencillez, y ayudar a los
demás tanto como pudiera. Aunque fue criticado
fuertemente por su influencia sobre mí, él siguió a su
conciencia y sólo me hablaba con la verdad.
